package connectmodel;

/**
 * PieceType Enum holds the different types of pieces available for player
 * @author hudson
 *
 */
public enum PieceType 
{
	RED ("Red"),
	BLACK ("Black"),
	GREEN ("Green"),
	YELLOW ("Yellow");
    
	private String myType;
	
    private PieceType(String type) 
    {
    	myType = type;
    }
    
    /**
     * Returns the myType string
     * @return
     */
    public String getType()
    {
    	return myType;
    }
}