package connectmodel;

/**
 * The player class takes in a String name and a PieceType type and creates a player to be
 * used in the GameEngine class.
 * @author hudson
 *
 */
public class Player 
{
	public static final String DEFAULT_NAME = "JohnCena";
	private String myName;
	private int myNumWins;
	private int myScore;
	private PieceType myPieceType;

	/**
	 * Creates a player instance with the given name and piece type
	 * @param name
	 * @param type
	 */
	public Player(String name, PieceType type)
	{
		if(validateName(name))
		{
			myName = name;
		} else
		{
			myName = DEFAULT_NAME;
		}
	  
	  myPieceType = type;
	  myNumWins = 0;
	  myScore = 0;
	  
	}

	/**
	 * Checks to see if a given name string contains only digits and letters and returns the default name if it does not.
	 * @param name
	 * @return
	 */
	private boolean validateName(String name) 
	{
		if (name != "")
		{
			for (int i = 0; i < name.length(); i++) 
			{
				if (!Character.isDigit(name.charAt(i)) && !Character.isAlphabetic(name.charAt(i)))
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Increments the score by 1
	 */
	public void incrementScore() 
	{
	  myScore += 1;
	}
	
	/**
	 * Returns the piece type
	 * @return
	 */
	public PieceType getPieceType()
	{
		return myPieceType;
	}

	/**
	 * returns the player's name
	 * @return
	 */
	public String getName() 
	{
		return myName;
	}
	
	/**
	 * Increments the number of player wins by one 
	 */
	public void incrementNumWins() 
	{
		myNumWins += 1;
	}

	/**
	 * return the number of wins the player has
	 * @return
	 */
	public int getNumWins()
	{
		return myNumWins;
	}
	
	/**
	 * Return the player's current score
	 * @return
	 */
	public int getScore()
	{
		return myScore;
	}
}